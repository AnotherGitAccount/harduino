# Harduino

## Project description

Harduino aims to mix the arduino project with FPGAs to make arduino projects more versatile. Arduino is
very convenient for prototyping and getting things done quickly but one still often needs to provide
additional hardware. What if this hardware could be provided by the board itself, within its FPGA? That's
what we are looking for, making a fully self-suficiant development board for prototypers.

## Steps

All these steps must be answered and documented on the wiki.

1. ### Documentation
    - [ ] Chosing an FPGA that includes an hardware CPU (idealy compatible with [Symbiflow](https://symbiflow.github.io/))
    - [ ] How to flash the FPGA?
    - [ ] What is arduino exactly? What is to be flashed?
    - [ ] How to compile arduino for the CPU of the FPGA?

2. ### PCB

3. ### Testing

4. ### Automating the flash process

5. ### Arduino and FPGA libraries





